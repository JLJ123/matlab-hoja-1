%% Prácticas de Matlab
%% Métodos multipaso
%% Hoja 6
% *Nombre:*
% 
% *Apellido:*
% 
% *DNI:*
%% 
% %% Práctica 1 Ecuaciones en diferencias 
% Tomando como datos $x_{0}=1, \quad x_1=1.01,\quad N=100$, calculen los términos 
% de la sucesión
% 
% $$    \begin{cases}      x_{n+2} & = {7 \over 3} x_{n+1} - {2 \over 3} x_{n} 
% \\      & x_{0}, \ x_{1} \quad \mbox{dados}    \end{cases}$$
% 
% para $n=0, \ldots, N$. Los resultados han de almacenarse en la tabla $x$. 
% Además haz una gráfica de $x_{n}$ contra $n$.
% 
% *Solución:*
x = [1,1.01];
N = 100;
nvec = 1:1:100;
for n = 3:N
    x(n) = (7/3)*x(n-1) -(2/3)*x(n-2);
end
figure(1)
p1 = plot(nvec,x);
s = sprintf('x0=1 x1=1.01 N=100');
p1.Marker = '*';
title(s)
%% Práctica 2 Leap frog (Ecuación escalar)
% Consideramos el método de Leap-Frog (punto medio).
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerad la EDO
% 
% $$\begin{cases}y^{\prime}&=\lambda y\\y(0)&=1\\\end{cases}$$
% 
% $\lambda = -20$, resolved dicha EDO con el metodo de Leap-frog (usando el 
% método de Euler modificado para inicializarlo), con $N=100$, $N=1000$, y $N=10000$. 
% Pintad la solucion $y$ frente a $t$.
% 
% *Solución:*
k = -20;
f = @(t,y) k*y;
y0 = 1;
N = 100;
intv = [0,1];
[t1,y1] = leap_frog(f,intv,y0,N);
figure(2)
p2 = plot(t1,y1);
s = sprintf('intv=[0,1] y(0)=1,\n t vsy,\n met=leapfrog lamda=-20, N=100');
title(s)
legend('leapfrog')
xlabel('y')
ylabel('y')

[t2,y2] = leap_frog(f,intv,y0,N*10);
figure(3)
p3 = plot(t2,y2);
s = sprintf('intv=[0,1] y(0)=1,\n t vsy,\n met=leapfrog lamda=-20, N=1000');
title(s)
legend('leapfrog')
xlabel('y')
ylabel('y')

[t3,y3] = leap_frog(f,intv,y0,N*100);
figure(4)
p4 = plot(t3,y3);
s = sprintf('intv=[0,1] y(0)=1,\n t vsy,\n met=leapfrog lamda=-20, N=10000');
title(s)
legend('leapfrog')
xlabel('y')
ylabel('y')


%% Práctica 3 Leap frog (Sistemas de ecuaciones) 
% Consideramos el método de Leap-frog.
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
%% 
% * Haz un diagrama de eficiencia (solo para $h$) en la misma manera como en 
% la hoja anterior 
%% 
% *Solución:*
g = @(t) [2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
f = @(t,y) [-2*y(1)+y(2)+2*sin(t);y(1)-2*y(2)+2*(cos(t)-sin(t))];
intv = [0,10];
y0 = [2,3];
N0 = 100;
h0 = 0.1;
Nvec = N0;
hvec = h0;
for n = 2:8
    Nvec(n) = 2*Nvec(n-1);
    hvec(n) = hvec(n-1)/2;
end

error = NaN;
for n = 1:8
    [t,y] = leap_frog(f,intv,y0,Nvec(n));
    dif = y - g(t);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error(n) = error_n;
end
figure(5)
p5 = loglog(Nvec,error);
legend('leapfrog')
s = sprintf('Error maximo vs N,\n met=leapfrog intv=[0,10]');
title(s)
xlabel('evaluaciones')
ylabel('Error maximo')


%% 
% %% 
% * además  $N=1000$ dibuja el error  (es decir $\log\left(\|y(t_n)-y_n\|_{\infty}\right)$ 
% pero no $\log\left(\max(\max(|(y(t_n-y_n)|)))\right)$) frente la variable $t$.
%% 
% *Solución:*

[t4,y4] = leap_frog(f,intv,y0,1000);
log_error = NaN;
for n=1:1001
    dif = g(t4(n)) - y4(n);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    log_error(n) = log(error_n);
end
figure(6)
p6 = plot(t4,log_error);





%% Práctica 5 BDF 
% Implementa el método *BDF*
% 
% $$  y_{n+2}-\frac{4}{3} y_{n+1}+\frac{1}{3} y_n =\frac{2}{3} h f_{n+2}\,.$$
% 
% *Observación:*
%% 
% * Inicializa el método con un método implícito del mismo orden.
% * En cada paso tienes que resolver una ecuación implícita  $z=g(h,x,z)$. Usa 
% la idea de iteración tipo Newton.
%% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haz un diagrama de eficiencia (solo para $h$) en la misma manera como en la 
% practica anterior
% 
% *Solución:*

f = @(t,y) [-2*y(1)+y(2)+2*sin(t);998*y(1)-999*y(2)+999*(cos(t)-sin(t))];
jfunc = [-2,1;998,-999];
y0 =[2,3];
nmax = 10;
tol = 0.001;
intv = [0,10];
Nvec = NaN;
Nvec(1) = 200;
hvec = NaN;
hvec(1) = 0.05;
for n = 2:8
    Nvec(:,n)=Nvec(n-1)*2;
    hvec(:,n)=hvec(n-1)/2;
end
error_BDF = NaN;
for n = 1:8
    [t,y] = BDF(f,jfunc,intv,y0,Nvec(n),nmax,tol);
    dif = y - g(t);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_BDF(n) = error_n;
end

figure(7)
p7 = plot(hvec,error_BDF);


%% Apéndice: Las funciones Leap_frog y BDF2

function [t,y] = leap_frog(f,intv,y0,N)
y = y0(:);
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
y(:,2) = y(:,1)+h*f(t(1)+h/2,y(:,1)+(h/2)*f(t(1),y(:,1)));
for n = 3:N+1
    y(:,n) = y(:,n-2) + 2*h*f(t(n-1),y(:,n-1));
end
end


function [t,y] = BDF(f,jfunc,intv,y0,N,nmax,tol)
y = y0(:);
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
for n=1:2
    yk = y(:,n);
    tk = t(n);
    yk0 = yk + h*f(tk,yk);
    k = 0;
    error = tol+1;
    while k < nmax && error > tol
        f0 = yk + h*f(tk+h,yk0);
        yk1 = yk + (h/2)*(f(tk,yk)+f(tk+h,f0));
        error = norm(yk0-yk1);
        yk0 = yk1;
        k = k+1;
    end
    y(:,n+1) = y(:,n) + h*f(tk+h,yk0);
end
for n =3:N
    x0 = y(:,n-1);
    x1 = y(:,n);
    error = tol + 1;
    k = 0;
    x2 = x1 + h*f(t(n),x1);
    while k < nmax && error > tol
        x3 =  x2 - jfunc\f(t(n),x2);
        error = norm(x3-x2);
        x2 = x3;
        k = k+1;
    end
    y(:,n+1) = (4/3)*x1 - (1/3)*x0 + (2/3)*h*f(t(n+1),x2);
end
end

