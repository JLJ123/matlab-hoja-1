%% Prácticas de Matlab
%% Método de disparo
%% Hoja 9
% *Nombre:*
% 
% *Apellido:*
% 
% *DNI:*
% 
% *Email:*
%% 
% % 
% %% Práctica 1 (Implementación del método del disparo lineal)
% Crea una función  *midisplin.m* que, tomando como datos los coeficientes  
% $p(t)$,  $q(t)$, $r(t)$ de la ecuación diferencial, implemente el método del 
% disparo lineal para cada una de las condiciones de contorno siguientes:
% 
% $$    \begin{array}{ccc}  x(t_{0})  &=&a, \qquad x(T)=b\\  x'(t_{0}) &=&a, 
% \qquad x(T)=b\\  x(t_{0})  &=&a, \qquad x'(T)=b\\  x'(t_{0}) &=&a, \qquad x'(T)=b    
% \end{array}$$
% 
% y que responda a la sintaxis
% 
% |[t,u]=midisplin(p,q,r,interval,a,b,N,c1,c2)|
% 
% *Indicación*: utiliza dos variables de entrada adicionales $C1$ y $C2$ respectivamente 
% para indicar cuál es la condición de contorno prescrita en $t_{0}$ y $T$ respectivamente. 
% Utiliza el convenio de que, cuando uno de estos parámetros es 0, entonces estamos 
% prefijando el valor de la función, mientras que si es 1 entonces estamos prefijando 
% el valor de la derivada. Como método de resolución de los problemas de valor 
% inicial, emplea el método de Runge--Kutta de orden 4.
%% Práctica 2 Resolución (D-D)
% Utiliza las implementaciones anteriores para resolver los siguientes problemas 
% de contorno. Explora las soluciones modificando las condiciones de contorno. 
% (N=10)
% 
% $$x''(t)=4(x(t)-t), \quad 0\leq t\leq 1 \quad    x(0)=-5, x(1)=2$$
% 
% *Solución*

clear all
close all
disp('H7: cod UB P1')
p=@(t)[0*t];
q=@(t)[4+0*t];
r=@(t)[-4*t];
intv=[0 1];
a=-5;
b=2;
c1=0;
c2=0;
N=10;
[t1,u1]=midisplin(p,q,r,intv,a,b,N,c1,c2);
figure(1)
grid on
hold on
plot(t1,u1(1,:),'r-*'), 
plot(t1,u1(2,:),'b-*'), 
legend('solucion','derivada')
grid on
title('$x^{\prime\prime}(t)=4(x(t)-t) \quad 0\leq t\leq 1 \qquad     x(0)=0 \quad x(1)=2$','Interpreter','latex')
%% Práctica 3 Resolución (D-N)
% Utiliza las implementaciones anteriores para resolver los siguientes problemas 
% de contorno. Explora las soluciones modificando las condiciones de contorno. 
% (N=10)
% 
% $$$x''(t) =3x'(t) +2x(t)+3\cos(t), \quad 0\leq     t \leq 5, x(0)=-2, x'(5)=1$$
% 
% *Solución*


disp('H7: cod UB P2')
p=@(t)[3+0*t];
q=@(t)[2+0*t];
r=@(t)[3*cos(t)];
intv=[0 5];
a=-2;
b=1;
c1=0;
c2=1;
N=10;
[t1,u1]=midisplin(p,q,r,intv,a,b,N,c1,c2);
figure(2)
grid on
hold on
plot(t1,u1(1,:),'r-*'), 
plot(t1,u1(2,:),'b-*'), 
legend('solucion','derivada')
grid on
title('$x^{\prime\prime}(t) =3x^{\prime}(t) +2x(t)+3\cos(t) \quad     0\leq     t \leq 5, \quad x(0)=-2, \quad x^{\prime}(5)=1$','Interpreter','latex')
hold off
%% Práctica 4 Resolución (N-D)
% Utiliza las implementaciones anteriores para resolver los siguientes problemas 
% de contorno. Explora las soluciones modificando las condiciones de contorno. 
% (N=10)
% 
% $$x''(t) = \cos(t) x(t)+t, \quad 0\leq t \leq 10,    x'(0)=-2, x(10)=-1.$$
% 
% *Solución*


disp('H7:  cod UB P3')
p=@(t)[0*t];
q=@(t)[cos(t)];
r=@(t)[t];
intv=[0 10];
a=-2;
% Este también es b=-1, pero ahora todo genial!
b=1;
c1=1;
c2=0;
N=10;
[t1,u1]=midisplin(p,q,r,intv,a,b,N,c1,c2);
figure(3)
grid on
hold on
plot(t1,u1(1,:),'r-*'), 
plot(t1,u1(2,:),'b-*'), 
legend('solucion','derivada')
grid on

title('$x^{\prime\prime}(t) = \cos(t) x(t)+t, \quad 0\leq t \leq    10 \quad    x^{\prime}(0)=-2, \quad x(10)=-1$','Interpreter','latex')

hold off
%% Práctica 5 Resolución (N-N)
% Utiliza las implementaciones anteriores para resolver los siguientes problemas 
% de contorno. Explora las soluciones modificando las condiciones de contorno. 
% (N=10)
% 
% $$$x''(t) =(1-\sin(t))x'(t) +\cos(t) x(t)+\sin(t),    \quad 0\leq t \leq 1, 
% x'(0)=-2, x'(10)=-1.$$
% 
% *Solución:*


disp('H7:  cod UB P4')
p=@(t)[1-sin(t)];
q=@(t)[cos(t)];
r=@(t)[sin(t)];
intv=[0 10];
a=-2;
b=-1;
c1=1;
c2=1;
N=10;
[t1,u1]=midisplin(p,q,r,intv,a,b,N,c1,c2);
figure(4)
grid on
hold on
plot(t1,u1(1,:),'r-*'), 
plot(t1,u1(2,:),'b-*'), 
legend('solucion','derivada')
grid on
title('$x^{\prime\prime}(t) =(1-\sin(t))x^{\prime}(t) + \cos(t) x(t) + \sin(t) \quad 0\leq t \leq 1 \quad x^{\prime}(0)=-2, \quad x^{\prime}(10)=-1$','Interpreter','latex')
hold off
%% Apéndice

function [t,u]=midisplin(p,q,r,interval,a,b,N,c1,c2)
h = (interval(2)-interval(1))/N;
t = interval(1):h:interval(2);
f1 = @(t,y) [y(2);p(t)*y(2)+q(t)*y(1)+r(t)];
f2 = @(t,y) [y(2);p(t)*y(2)+q(t)*y(1)];
if c1 == 0
    x0 = [a,0];
    x1 = [0,1];
end
if c1 ==1 
    x0 = [0,a];
    x1 = [1,0];
end
[t1,y1] = mirk4(f1,interval,x0,N);
[t2,y2] = mirk4(f2,interval,x1,N);

% Hasta aquí bien: creas los dos problemas adicionales, y los resuelves con RK4, obteniendo las soluciones y1, y2.
% Y efectivamente, hay que mirar el último valor de las soluciones numéricas y1, y2 (o bien y1', y2').
% Pero ahora te falla el cálculo de despejar s: ¡hay que mirar c2!

% - Si c2=0 (Dirichlet), entonces y(T)=y1(T)+sy2(T)=b
% - Si c2=1 (Neumann), entonces podedmos derivar lo anterior y obtener y'(T)=y1'(T)+sy2'(T).

% En función de cada caso estaremos en una manera diferente de obtener la s, y por tanto la solución y(t)=y1(t)+sy2(t).

% Sin embargo, observa que en el cálculo de la s pueden pasar varias cosas:
% - Si el denominador no es cero, todo bien: solución única y(t)=y1(t)+sy2(t)
% - Si el denominador es cero, pero el numerador no: hay infinitas soluciones
% - Si el denominador y el numerador son cero, no hay solución

% Aunque en particular estos ejercicios tienen todos una solución, tienes que contemplar también los demás casos!

k = length(t);
y1k = y1(:,k);
y2k = y2(:,k);
if c2 == 0
    if y2k(1) ~= 0
        s = (b-y1k(1))/y2k(1);
        if c1 == 0
            y0 = [a,s];
        end
        if c1 ==1 
            y0 = [s,a];
        end
        u = y0(:);
        for  i=2:N+1
            u(:,i) = y1(:,i) + s*y2(:,i);
        end
    end
    if y2k(1) == 0
        if b - y1k(1) == 0
            disp('no hay solucion')
        end
        if b - y1k(1) ~= 0
            disp('hay infinitas soluciones')
        end
    end
end

if c2 == 1
    if y2k(2) ~= 0
        s = (b-y1k(2))/y2k(2);
        if c1 == 0
            y0 = [a,s];
        end
        if c1 ==1 
            y0 = [s,a];
        end
        u = y0(:);
        for  i=2:N+1
            u(:,i) = y1(:,i) + s*y2(:,i);
        end
    end

    if y2k(2) == 0
        if b - y1k(2) == 0
            disp('no hay solucion')
        end
        if b - y1k(2) ~=0
            disp('hay infinitas soluciones')
        end

    end
end

end


function [t,y]=mirk4(f,intv,y0,N)
y = y0(:);
h = (intv(2)-intv(1))/N;
t = intv(1);
for n=2:N+1
    t(:,n) = t(n-1)+h;
    f1 = f(t(n-1),y(:,n-1));
    f2 = f(t(n-1)+h/2,y(:,n-1)+(h/2)*f1);
    f3 = f(t(n-1)+h/2,y(:,n-1)+(h/2)*f2);
    f4 = f(t(n-1)+h,y(:,n-1)+h*f3);
    y(:,n) = y(:,n-1)+(h/6)*(f1+2*f2+2*f3+f4);
end
end
