%% Practicas de Matlab
%% Metodos adaptativos
%% Hoja 7
% *Nombre:*
% 
% *Apellido:*
% 
% *DNI:*
% 
% *Email:*
%% 
% %% *Par encajado*
% $h_{op}$ está dado por 
% 
% $$      h_{opt}=\mbox{mín}      \left(        \mbox{{ HMAX}},h_{n}\mbox{mín}\left(\mbox{{FACMAX,FAC}}          
% \left(            \frac{TOL    h_{n}}{\mathrm{Error}}          \right)^{\frac{1}{p+1}}        
% \right)      \right)$$
% 
% _FACMAX_ se suele tomar entre $1.5$ y $5$. 
% 
% %% 
%% Práctica 1: (Runge-Kutta Fehlberg-4-5) 
% La tabla de RKF-45 está dada por
% 
% $$    \begin{array}{c|cccccc}      0 &  & & & & & \\[0.2cm]      \frac{1}{4}& 
% \frac{1}{4} & & & & & \\[0.2cm]      \frac{3}{8}& \frac{3}{32}& \frac{9}{32} 
% &  & & &\\[0.2cm]      \frac{12}{13} & \frac{1932}{2197} & -\frac{7200}{2197} 
% &      \frac{7296}{2197} & & & \\[0.2cm]      1 & \frac{439}{216} & -8 & \frac{3680}{513} 
% & -\frac{845}{4104} & &\\[0.2cm]      \frac{1}{2}& -\frac{8}{27}& 2 & -\frac{3544}{2565} 
% & \frac{1859}{4104}      & -\frac{11}{40} & \\[0.2cm]      \hline      & \frac{25}{216} 
% & 0 & \frac{1408}{2565} & \frac{2197}{4104} &      -\frac{1}{5}      & 0\\[0.2cm]      
% & \frac{16}{135} & 0 & \frac{6656}{12825} & \frac{28561}{56430} & -\frac{9}{50}        
% & \frac{2}{55}\\[0.2cm]      \hline      & \frac{1}{360}& 0 & -\frac{128}{4275} 
% & - \frac{2197}{75240}&      \frac{1}{50} & \frac{2}{55}    \end{array}$$
% 
% con el error:
% 
% $$    ERR = h \left| \frac{1}{360} k_1  - \frac{128}{4275} k_3 -      \frac{2197}{75240} 
% k_4 + \frac{1}{50} k_5 + \frac{2}{55} k_6\right| . $$
% 
% Implementa dicho método llamando la funcion *mirk45fehlberg* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=mirk45fehlberg(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01;
%% 
%% Práctica 2 (Euler mejorado-Euler (2-1) (método de extrapolación) 
% Consideramos el siguiente método de extrapolación local con el tablero:
% 
% $$    \begin{array}{l|l}      \begin{array}{l}        {0}	\\        {1}      
% \end{array}      &	       \begin{array}{ll}        0 & 0 \\        1 & 0      
% \end{array}      \\      \hline      y_{n+1}&      \begin{array}{cc}        
% \frac{1}{2}& \frac{1}{2}	      \end{array}      \\      \hline      \hat{y}_{n+1}&      
% \begin{array}{cc}        1& 0      \end{array}    \end{array}$$
% 
% que
%% 
% * avanza con el método de Euler mejorado y
% * estima con el método de Euler.
% * en este caso el método de avance es de orden 2, pero a cambio hay que hacer 
% dos evaluaciones de función por paso.
%% 
% Implementa dicho método llamando la funcion *mieuler21* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=mieuler21(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01;
%% Práctica 3: FSAL (Euler-Euler-mejorado (1-2))
% $$      \begin{array}{l|l}        \begin{array}{l}          {0}	\\          
% {1}        \end{array}        &	         \begin{array}{ll}          0 & 0 \\          
% 1 & 0        \end{array}        \\        \hline        y_{n+1}&        \begin{array}{cc}          
% 1& 0        \end{array}        \\        \hline        \hat{y}_{n+1}&        
% \begin{array}{cc}          \frac{1}{2}& \frac{1}{2}        \end{array}      
% \end{array}$$
%% 
% * avanza con el método de Euler y
% * estima con el método de Euler mejorado.
%% 
% Implementa dicho método llamando la funcion *mieuler12fsal* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=mieuler12fsal(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01
% * Usad la propiededad *FSAL*
%% Práctica 4 (Método adaptativo de Dormand-Prince FSAL)
% Su tabla está dada por 
% 
% $$\begin{array}{c|ccccccc}0 \\\displaystyle \frac{1}{5} & \displaystyle \frac{1}{5}               
% \\	 \displaystyle \frac{3}{10}     & \displaystyle \frac{3}{40} & \displaystyle 
% \frac{9}{40} \\	 \displaystyle \frac{4}{5} &       \displaystyle \frac{44}{45} 
% & -\displaystyle \frac{56}{15} & \displaystyle \frac{32}{9} \\ \displaystyle 
% \frac{8}{9} & \displaystyle \frac{19372}{6561}&  -\displaystyle \frac{25360}{2187} 
% & \displaystyle \frac{64448}{6561}& -\displaystyle \frac{212}{729} \\1 &      
% \displaystyle \frac{9017}{3168}& -\displaystyle \frac{355}{33}& \displaystyle 
% \frac{46732}{5247}&     \displaystyle \frac{49}{176}&  -\displaystyle \frac{5103}{18656}  
% \\	1 &      \displaystyle \frac{35}{384}&  0&  \displaystyle \frac{500}{1113}& 
% \displaystyle \frac{125}{192}&     -\displaystyle \frac{2187}{6784}& \displaystyle 
% \frac{11}{84}	\\\hline y_{1}&	  \displaystyle \frac{35}{384}& 0& \displaystyle 
% \frac{500}{1113}& \displaystyle \frac{125}{192}& -\displaystyle \frac{2187}{6784}& 
% \displaystyle \frac{11}{84}&  0	\\\hline\widehat  y_{1}&  \displaystyle \frac{5179}{57600}&0&  
% \displaystyle \frac{7571}{16695} & \displaystyle \frac{393}{640} &\displaystyle 
% \frac{92097}{339200}  & \displaystyle \frac{187}{2100} &  \displaystyle \frac{1}{40}	
% \\\end{array}$$
% 
% Implementa dicho método llamando la funcion *midp45sal* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=midp45fsal(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01
% * Usad la propiededad *FSAL*
%% Aplicación
% Práctica 5 (Solución que explota)
% Considera el PVI
% 
% $$  \begin{cases}    x'(t)&=x^2(t)\\     x(0)&=1    \end{cases} $$
% 
% La solución exacta es
% 
% $$   x(t)=\frac{1}{1-t}$$
% 
% que es no acotada cuando $t \to 1$.
%% 
% * Usando el método de _Euler explicito_  resuelve el problema en el intervalo  
% $[0 \quad 2]$.
% * Utiliza ahora los 4 metodos adaptativos. 
% * ¿Qué sucede cerca de la discontinuidad que aparece en $t=1$?
%% 
% *Solución*



%% 
% % Práctica 6 (Ecucacion rigida) 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haz un diagrama de eficiencia en la misma manera como en la *hoja1,* con las 
% siguientes diferencias.
%% 
% * Para hacer un diagrama de eficiencia para un método adaptativo cambia la 
% tolerancia, empezando con $TOL_{initial}=0.01$ y repite el calculo con $TOL_{nuevo}=  
% TOL/2$.
% * comparando el método (con paso fijo) del trapecio (con Newton) con  *mieuler12.m* 
% y *mieuler21.m*.
%% 
% *Solucion*
f = @(t,x) x^2;
g = @(t) 1/(1-t);
facmax = 5;
fac = 0.9;
TOL = 0.001;
y0 = 1;
hmin = 10^(-5);
intv = [0,2];
hmax = (intv(2)-intv(1))/50;
[t,y,hchng_vec] = mirkfehlb45(f,intv,y0,TOL,hmin,hmax,fac,facmax);

figure(1)
plot(t,y)
legend('RKFehlberg45')
xlabel('t')
ylabel('y')
grid on

[t1,y1,hchng_vec1,err_vec] = mieuler21(f,intv,y0,TOL,hmin,hmax,fac,facmax);
figure(2)
plot(t1,y1)
legend('mieuler21')
xlabel('t')
ylabel('y')
grid on


[t2,y2,hchng_vec2] = mieuler12fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax);
figure(3)
plot(t2,y2)
legend('mieuler12')
xlabel('t')
ylabel('y')
grid on


[t3,y3,hchng_vec3] = midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax);
figure(4)
plot(t3,y3)
legend('Dormand-Prince')
xlabel('t')
ylabel('y')
grid on



f = @(t,y) [-2*y(1) + y(2) + 2*sin(t); 998*y(1) - 999*y(2) + 999*(cos(t) - sin(t))];
g = @(t, y) [2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv = [0,10];
y0 = [2,3];
tol_vec = NaN;
tol_vec(1) = 0.01;
[t,y,hchng_vec,err_vec] = mieuler21(f,intv,y0,0.01,hmin,hmax,fac,facmax);
err_vec2 = NaN;
err_vec2(1) = max(err_vec);
for i = 2:8
    tol_vec(i) = tol_vec(i-1)/2;
    [t,y,hchng_vec,err_vec] = mieuler21(f,intv,y0,tol_vec(i),hmin,hmax,fac,facmax);
    err_vec2(i) = max(err_vec);
end

figure(5)
plot(tol_vec,err_vec2)

[t,y,hchng_vec,err_vec] = mieuler12fsal(f,intv,y0,0.01,hmin,hmax,fac,facmax);
err_vec3 = NaN;
err_vec3(1) = max(err_vec);
for i = 2:8
    [t,y,hchng_vec,err_vec] = mieuler12fsal(f,intv,y0,tol_vec(i),hmin,hmax,fac,facmax);
    err_vec3(i) = max(err_vec);
end
figure(6)
plot(tol_vec,err_vec3)


%% Apendice: Las funciones

function [t,y,hchng_vec]=mirkfehlb45(f,intv,y0,TOL,hmin,hmax,fac,facmax)
hchng_vec = NaN;
hchng_vec(1)=hmax;
y = y0(:);
t = intv(1);
i = 1;
while max(t) < intv(2) && min(hchng_vec) > hmin
    h = hchng_vec(i);
    k1 = f(t(i),y(:,i));                                                  
    k2 = f(t(i)+h/4,y(:,i)+h*k1/4);                                     
    k3 = f(t(i)+3*h/8, y(:,i)+h*((3/32)*k1 + (9/32)*k2));                     
    k4 = f(t(i)+12*h/13, y(:,i)+h*((1932/2197)*k1 - (7200/2197)*k2 + (7296/2197)*k3));   
    k5 = f(t(i)+h, y(:,i)+h*((439/216)*k1 - 8*k2 + (3680/513)*k3 - (845/4104)*k4));
    k6 = f(t(i)+h/2, y(:,i)+h*((-8/27)*k1 + 2*k2 - (3544/2565)*k3 + (1859/4104)*k4 - (11/40)*k5));
    y(:,i+1) = y(:,i) +h*((25/216)*k1 + 1408*k3/2565 +2197*k4/4104 -k5/5);
    error = h*norm((1/360)*k1 - (128/4275)*k3 -(2197/75240)*k4 +k5/50 +2*k6/55);
    hnew = h;
    if h > hmin
        hnew = min(hmax,h*min(facmax,fac*(h*TOL/error)^(1/5)));
    end
    hchng_vec(i+1) = hnew;
    t(i+1) = t(i) + hnew;
    i = i +1;
end
end

%%
function [t,y,hchng_vec,err_vec]=mieuler21(f,intv,y0,TOL,hmin,hmax,fac,facmax)
hchng_vec = NaN;
hchng_vec(1)=hmax;
err_vec = NaN;
err_vec(1) = 0;
y = y0(:);
t = intv(1);
i = 1;
while max(t) < intv(2) && min(hchng_vec) > hmin
    h = hchng_vec(i);
    k1 = f(t(i),y(:,i));
    k2 = f(t(i)+h,y(:,i)+h*k1);
    y(:,i+1) = y(:,i)+(h/2)*(k1+k2);
    error = h*norm(k1/2-k2/2);
    err_vec(i) = error;
    hnew = h;
    if h > hmin
        hnew = min(hmax,h*min(facmax,fac*(h*TOL/error)^(1/3)));
    end
    hchng_vec(i+1) = hnew;
    t(i+1) = t(i) + hnew;
    i = i +1;
end
end
%% 
% 
function [t,y,hchng_vec,err_vec]=mieuler12fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
hchng_vec = NaN;
hchng_vec(1)=hmax;
err_vec = NaN;
err_vec(1) = 0;
y = y0(:);
t = intv(1);
i = 1;
while max(t) < intv(2) && min(hchng_vec) > hmin
    h = hchng_vec(i);
    k1 = f(t(i),y(:,i));
    k2 = f(t(i)+h,y(:,i)+h*k1);
    y(:,i+1) = y(:,i)+h*k1;
    error = h*norm(k1/2-k2/2);
    err_vec(i) = error;
    hnew = h;
    if h > hmin
        hnew = min(hmax,h*min(facmax,fac*(h*TOL/error)^(1/3)));
    end
    hchng_vec(i+1) = hnew;
    t(i+1) = t(i) + hnew;
    i = i +1;
end
end
%% 
% 


function [t,y,hchng_vec]=midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
hchng_vec = NaN;
hchng_vec(1)=hmax;
y = y0(:);
t = intv(1);
i = 1;
while max(t) < intv(2) && min(hchng_vec) > hmin
    h = hchng_vec(i);
    tk = t(i);
    yk = y(:,i);
    k1 = f(tk,yk);
    k2 = f(tk+h/5,yk+h*k1/5);
    k3 = f(tk+3*h/10,yk+h*(3*k1/40 + 9*k2/40 ));
    k4 = f(tk+4*h/5,yk+h*(44*k1/45-56*k2/15+32*k3/9));
    k5 = f(tk+8*h/9,yk+h*(19372*k1/6561-25360*k2/2187+64448*k3/6561-212*k4/729));
    k6 = f(tk+h, yk+h*(9017*k1/3168-355*k2/33+46732*k3/5247+49*k4/176-5103*k5/18656));
    k7 = f(tk+h, yk+h*(35*k1/384+500*k3/113+125*k4/192-2187*k5/6784+11*k6/84));
    y(:,i+1) = yk + h*(35*k1/384+500*k3/113+125*k4/192-2187*k5/6784+11*k6/84);
    error = h*norm(71*k1/57600-71*k3/16695+71*k4/1920-17253*k5/339200+22*k6/525-k7/40);
    hnew = h;
    if h > hmin
        hnew = min(hmax,h*min(facmax,fac*(h*TOL/error)^(1/5)));
    end
    hchng_vec(i+1) = hnew;
    t(i+1) = t(i) + hnew;
    i = i +1;
end

end