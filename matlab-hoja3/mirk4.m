function [t,y]=mirk4(f,intv,y0,N)
y = y0(:);
h = (intv(2)-intv(1))/N;
t = intv(1);
for n=2:N+1
    t(:,n) = t(n-1)+h;
    f1 = f(t(n-1),y(:,n-1));
    f2 = f(t(n-1)+h/2,y(:,n-1)+(h/2)*f1);
    f3 = f(t(n-1)+h/2,y(:,n-1)+(h/2)*f2);
    f4 = f(t(n-1)+h,y(:,n-1)+h*f3);
    y(:,n) = y(:,n-1)+(h/6)*(f1+2*f2+2*f3+f4);
end
end
