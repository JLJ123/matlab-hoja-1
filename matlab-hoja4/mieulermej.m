function [t,y]=mieulermej(f,intv,y0,N)
y = y0(:);
h = (intv(2)-intv(1))/N;
t = intv(1);
for n=2:N+1
    t(:,n) = t(n-1)+h;
    f1 = f(t(n-1),y(:,n-1));
    f2 = f(t(n),y(:,n-1)+h*f1);
    y(:,n) = y(:,n-1)+(h/2)*(f1+f2);
end
end
