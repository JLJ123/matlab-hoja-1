%% Prácticas de Matlab
%% Diagrama de eficiencia con métodos monopaso explícitos
%% Hoja 4
% *Nombre:*
% 
% *Apellido:*
% 
% *DNI:*
% 
% *Email:*
%% 
% %% 1. Diagrama de eficiencia
% Práctica 1 (El método de Euler explícito) 
% Consideramos el siguiente problema lineal
% 
% $$   y^{\prime}(t)=Ay(t)+B(t) \quad\mbox{para} \quad 0\leq t\leq 10,\quad  
% y(0)=(2,3)^{T},$$
% 
% $$    A=\left(\begin{array}{cc}        -2 & 1\\        1 & -2      \end{array}\right)    
% \qquad    B(t) =\left(\begin{array}{l}        2\sin(t)\\        2(\cos(t)-\sin(t)      
% \end{array}\right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}\left(\begin{array}{l}      1\\      1    \end{array}\right)  
% +  \left(\begin{array}{l}      \sin(t)\\      \cos(t)    \end{array}\right)$$
% 
% Se pide lo siguiente
%% 
% # Resuelve este sistema mediante el método de _Euler explícito,_ almacena 
% el máximo en valor absoluto de la diferencia entre la solución exacta y la solución 
% numérica calculada.  *Indicación:* piensa qué norma vas a usar, dependiendo 
% del tipo de salida (vector columna o vector fila) que haya producido tu algoritmo. 
% Efectúa este cálculo para varias elecciones
% # del paso $h_j$ con $j=0,\ldots,7$ siendo $h_0=0.1$, $h_j=\frac{h_0}{2^j}$. 
% Almacena los diferentes valores de $h_i$ en un vector $h_{vect}$.
% # del número de puntos $N$ siendo $N_0=100$, $N_i=2^{i}N_0$. Almacena los 
% diferentes valores de $N_i$ en un vector $N_{vect}$.
% # número de las evaluaciones totales $Ev_i$ que realiza cada algoritmo para 
% cada valor de $h_i$. Almacena los valores en un vector $Ev_{vect}$.
% # Almacena los distintos errores en un vector de nombre  *error_euler*
%% 
% Además
%% 
% * Dibuja, en una misma ventana, en escala logarítmica, el error almacenado 
% en el apartado anterior frente al paso $h$,  $h_{vect}$ *Indicación:* usa el 
% comando |loglog| en vez del comando |plot|. No use los comandos hold on, hold 
% off
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $N_{vect}$ 
% * Calcula la pendiente da la recta.
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $Ev_{vect}$.
% * Interpreta el resultado.
%% 
% %  Práctica 2 (Euler mejorado) 
% Repite el apartado anterior con el método de Euler mejorado 
% 
% % Práctica 3 (Euler modificado)
% Repite el apartado anterior con el método de Euler modificado
% 
% % Práctica 4 (Runge-Kutta 4)
% Repite el apartado anterior con el método de Runge-Kutta de orden 4.
% 
% % 
% % 
% *OJO:*  pon siempre el diagrama de eficiencia de Euler, Euler modificado, 
% Euler mejorado y Runge Kutta 4 en una gráfica como por ejemplo:
% 
% % 
% % 
% % 
% *Solución:*
g = @(t) [2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
f = @(t,y) [-2*y(1)+y(2)+2*sin(t);y(1)-2*y(2)+2*(cos(t)-sin(t))];
intv = [0,10];
y0 = [2,3];
N0 = 100;
h0 = 0.1;
Nvec = N0;
hvec = h0;
for n = 2:8
    Nvec(n) = 2*Nvec(n-1);
    hvec(n) = hvec(n-1)/2;
end


%Euler explicito:
Evec_euler = Nvec;
error_euler = NaN;

for n = 1:8
    [t,y] = mieuler(f,intv,y0,Nvec(n));
    dif = y - g(t);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_euler(n) = error_n;

end

%Euler mejorado:
Evec_eulermej = 2*Nvec;
error_eulermej = NaN;

for n = 1:8
    [t,y] = mieulermej(f,intv,y0,Nvec(n));
    dif = y - g(t);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_eulermej(n) = error_n;

end


%Euler modificado:

Evec_eulermod = 2*Nvec;
error_eulermod = NaN;

for n = 1:8
    [t,y] = mieulermod(f,intv,y0,Nvec(n));
    dif = y - g(t);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_eulermod(n) = error_n;

end

%Runge-Kutta 4:

Evec_mirk4 = 4*Nvec;
error_mirk4 = NaN;

for n = 1:8
    [t,y] = mirk4(f,intv,y0,Nvec(n));
    dif = y - g(t);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_mirk4(n) = error_n;

end

figure(1)
p = loglog(hvec,error_euler,hvec,error_eulermej,hvec,error_eulermod,hvec,error_mirk4);
legend('Metodo de Euler','Metodo de Euler mejorado','Metodo de Euler modificado', 'RK4')
p(1).Marker = '*';
p(2).Marker = '*';
p(3).Marker = '>';
p(4).Marker = 'diamond';
s = sprintf('Error maximo vs h,\n Problema no stiff,\n intv=[0 10] y0=[2 3],\n Nint=200 M=7');
title(s)
xlabel('h')
ylabel('Error maximo')

figure(2)
p1 = loglog(Nvec,error_euler,Nvec,error_eulermej,Nvec,error_eulermod,Nvec,error_mirk4);
legend('Metodo de Euler','Metodo de Euler mejorado','Metodo de Euler modificado', 'RK4')
p1(1).Marker = '*';
p1(2).Marker = '*';
p1(3).Marker = '>';
p1(4).Marker = 'diamond';
s1 = sprintf('Error maximo vs N,\n Problema no stiff,\n intv=[0 10] y0=[2 3],\n Nint=200 M=7');
title(s1)
xlabel('N')
ylabel('Error maximo')

figure(3)
p2 = loglog(Evec_euler,error_euler,Evec_eulermej,error_eulermej,Evec_eulermod,error_eulermod,Evec_mirk4,error_mirk4);
legend('Metodo de Euler','Metodo de Euler mejorado','Metodo de Euler modificado', 'RK4')
p2(1).Marker = '*';
p2(2).Marker = '*';
p2(3).Marker = '>';
p2(4).Marker = 'diamond';
s1 = sprintf('Error maximo vs N,\n Problema no stiff,\n intv=[0 10] y0=[2 3],\n Nint=200 M=7');
title(s1)
xlabel('Numero de evaluaciones')
ylabel('Error maximo')

%% Apéndice código: funciones de Euler, Euler modificado, Euler mejorado y Runge-Kutta 4, para calcular y pintar el diagrama de eficiencia y el orden