%% Prácticas de Matlab
%% Bucles simples
%% Hoja 2
% *Nombre:* Longjian
% 
% *Apellido:* Jiang
% 
% *DNI:* Y3438822Z
%% Sucesiones escalares
% % 
% Consideramos las sucesiones
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} + h x_{n}^{2}\\  t_{n+1}&=& t_{n} 
% + h   \end{array}$$
% Práctica 1 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Usad índices para realizar 
% el bucle. Datos $x(1)=1$  $t(1)=1$  $h=0.1$ $N=10$ y $N=100$
% 
% *Solución:*
N1 = 10;
N2 = 100;
x1 = zeros(1,N1+1);
t1 = zeros(1,N1+1);
x2 = zeros(1,N2+1);
t2 = zeros(1,N2+1);

x1(1) = 1;
t1(1) = 1;
x2(1) = 1;
t2(1) = 1;

for n=2:N1+1 
    x1(n) = x1(n-1) + (1/N1)*(x1(n-1))^2;
    t1(n) = t1(n-1) + (1/N1);
end

for n=2:N2+1
    x2(n) = x2(n-1) + (1/N2)*(x2(n-1))^2;
    t2(n) = t2(n-1) + (1/N2);
end

figure(1)
plot(t1,x1);
hold on 
plot(t2,x2);
legend('N=10','N=100');
grid on
hold off

% Práctica 2 (Script: Bucle sin usar índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1$ 
% $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*

N1 = 10;
N2 = 100;
x1 = 1;
t1 = 1;
x2 = 1;
t2 = 1;


for n=2:N1+1 
    x1 = [x1,x1(n-1) + (1/N1)*(x1(n-1))^2];
    t1 = [t1,t1(n-1) + (1/N1)];
end

for n=2:N2+1
    x2 = [x2,x2(n-1) + (1/N2)*(x2(n-1))^2];
    t2 = [t2,t2(n-1) + (1/N2)];
end

figure(2)
plot(t1,x1);
hold on 
plot(t2,x2);
legend('N=10','N=100');
grid on
hold off

%% Sucesiones de varias componentes
% Consideramos las sucesiones 
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} - h y_{n}\\  y_{n+1}&=& y_{n} + h 
% x_{n}\\  t_{n+1}&=& t_{n} + h   \end{array}$$
% Práctica 3 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1)=1,$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
N1 = 10;
N2 = 100;
x1 = zeros(1,N1+1);
t1 = zeros(1,N1+1);
y1 = zeros(1,N1+1);
x2 = zeros(1,N2+1);
t2 = zeros(1,N2+1);
y2 = zeros(1,N2+1);
x1(1) = 1;
y1(1) = 1;
t1(1) = 1;
x2(1) = 1;
y2(1) = 1;
t2(1) = 1;


for n=2:N1+1 
    x1(n) = x1(n-1) -(1/N1)*y1(n-1);
    y1(n) = y1(n-1) + (1/N1)*x1(n-1);
    t1(n) = t1(n-1) + (1/N1);
end

for n=2:N2+1
    x2(n) = x2(n-1) -(1/N2)*y2(n-1);
    y2(n) = y2(n-1) + (1/N2)*x2(n-1);
    t2(n) = t2(n-1) + (1/N2);
end

figure(3)
plot(x1,y1);
hold on 
plot(x2,y2);
legend('N=10','N=100');
grid on
hold off


% Práctica 4 (Script: Bucle sin usar índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1)=1,$ $t(1)=1,$ $h=0.1$,$ $N=10$ y $N=100$
% 
% *Solucion:*

N1 = 10;
N2 = 100;
x1 = 1;
y1 = 1;
t1 = 1;
x2 = 1;
y2 = 1;
t2 = 1;


for n=2:N1+1 
    x1 = [x1,x1(n-1) - (1/N1)*y1(n-1)];
    y1 = [y1,y1(n-1) + (1/N1)*x1(n-1)];
    t1 = [t1,t1(n-1) + (1/N1)];
end

for n=2:N2+1
    x2 = [x2,x2(n-1) - (1/N2)*y2(n-1)];
    y2 = [y2,y2(n-1) + (1/N2)*x2(n-1)];
    t2 = [t2,t2(n-1) + (1/N2)];
end

figure(4)
plot(x1,y1);
hold on 
plot(x2,y2);
legend('N=10','N=100');
grid on
hold off

%% Forma vectorial de las sucesiones
% Dadas las sucesiones: 
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} - h y_{n}\\  y_{n+1}&=& y_{n} + h 
% x_{n}\\  t_{n+1}&=& t_{n} + h   \end{array}$$
% 
% Escribid las dos primeras sucesiones en forma vectorial (en un papel o en 
% el propio mxl (usando el editor de ecuaciones).
% 
% *Solución:*
% Práctica 5 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script)_ abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Usad índices para realizar 
% el bucle. Datos $x(1)=1,$ $y(1)=1,$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*

N1 = 10;
N2 = 100;
x1 = zeros(1,N1+1);
t1 = zeros(1,N1+1);
y1 = zeros(1,N1+1);
x2 = zeros(1,N2+1);
t2 = zeros(1,N2+1);
y2 = zeros(1,N2+1);
x1(1) = 1;
y1(1) = 1;
t1(1) = 1;
x2(1) = 1;
y2(1) = 1;
t2(1) = 1;

z1 =[x1(1),y1(1)];
z2 =[x2(1),y2(1)];

for n=2:N1+1 
    x1(n) = x1(n-1) -(1/N1)*y1(n-1);
    y1(n) = y1(n-1) + (1/N1)*x1(n-1);
    z1 = [z1;[x1(n),y1(n)]];
    t1(n) = t1(n-1) + (1/N1);
end

for n=2:N2+1
    x2(n) = x2(n-1) -(1/N2)*y2(n-1);
    y2(n) = y2(n-1) + (1/N2)*x2(n-1);
    z2 = [z2;[x2(n),y2(n)]];
    t2(n) = t2(n-1) + (1/N2);
end

figure(5)
plot(x1,y1);
hold on 
plot(x2,y2);
legend('N=10','N=100');
grid on
hold off

% Práctica 6 (Script: Bucle sin usar índices)
% Escribid la instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1),$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
N1 = 10;
N2 = 100;
x1 = 1;
y1 = 1;
t1 = 1;
x2 = 1;
y2 = 1;
t2 = 1;

z1 =[x1,y1];
z2 =[x2(1),y2(1)];

for n=2:N1+1 
    x1 = [x1,x1(n-1) - (1/N1)*y1(n-1)];
    y1 = [y1,y1(n-1) + (1/N1)*x1(n-1)];
    z1 = [z1;[x1(n),y1(n)]];
    t1(n) = t1(n-1) + (1/N1);
end

for n=2:N2+1
    x2 = [x2,x2(n-1) - (1/N2)*y2(n-1)];
    y2 = [y2,y2(n-1) + (1/N2)*x2(n-1)];
    z2 = [z2;[x2(n),y2(n)]];
    t2(n) = t2(n-1) + (1/N2);
end

figure(6)
plot(x1,y1);
hold on 
plot(x2,y2);
legend('N=10','N=100');
grid on
hold off

%% Método de Euler
% Consideramos el método de Euler para el PVI:
% 
% $$  \begin{array}{cc}    \frac{dy}{dt}&=f(t,y)\\    y(t_0)&=\alpha  \end{array}$$
% 
% % 
% es decir
% 
% $$  y_{n+1}=y_n + h f(t_n,y_n).$$
% 
% Consideramos la ecuación diferencial (PVI)
% 
% $$  \begin{array}{cc}  \frac{d^2x}{dt^2}&=-x\\   x(0)&=1\\   \frac{dx(0)}{dt}&=1   
% \end{array}$$
% 
% Reescribid dicha ecuación como un sistema de ecuaciones y aplicad el método 
% de Euler. Escribid un _script _usando vuestros scripts anteriores (mejor implementar 
% sin índices) para resolver dicha EDO mediante el método de Euler. Pintad una 
% componente de la solución frente la otra.
% 
% Datos $x(1)=1,$ $\frac{dx(1)}{dt}=1,$ $t(1)=1$, $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
met ='Euler';
intv = [1 11];
x0 = [1;1];
x = 1;
y = 1;
t = 1;
N = 100;
h = 0.1;
z = [x,y];

for n =2:N+1
    x = [x,x(n-1)+h*y(n-1)];
    y = [y,y(n-1)-h*x(n-1)];
    z = [z;[y(n),x(n)]];
    t = [t,t(n-1)+h];
end

figure(7)
set(gca,'FontSize',16);
plot(y,x,'r-*');
grid on
s=sprintf('Ecuacion de pendulo,\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)


%% 
% |*Ultimo valor:*|
% 
% |*x=(-0.5603,   -2.2574)*|
% 
% |*Gráfica *|
% 
%