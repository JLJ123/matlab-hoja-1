%% Prácticas de Matlab
%% Resolución de EDO con métodos implícitos
%% Hoja 5 A
% *Nombre:* Longjian
% 
% *Apellido:* Jiang
% 
% *EMAIL:* longjian@ucm.es
% 
% *DNI:* Y3438822Z
% 
% %% 1. Implementación de métodos implícitos
%% Práctica 1 (Implementación del método de Euler implícito)
% Escribid en el apéndice A1 4 funciones que implementen el método de Euler 
% (implícito) 
% 
% $$      \left\{\begin{array}{l}               y_{i+1}=y_i + h f(t_{i+1},y_{i+1}) 
% \quad i=0,\ldots ,N-1          \\               y_0 \approx a        \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) en varias maneras 
% Existen las siguientes maneras de implementar dicho método, dependiendo de 
%% 
% * qué tipo de iteración usas para resolver la relación implícita 
% * cómo eliges el dato inicial para esa iteración.
%% 
% En cualquier caso, tenéis que implementar la iteración usando un
%% 
% * *while* y una tolerancia dada.
% * un numero máximo de iteraciones para evitar un bucle infinito.
%% 
% La iteración puede ser bien esto, bien esto
%% 
% * Una iteración simple.
% * Una iteración tipo Newton
%% 
% La elección del punto inicial para la iteración puede ser:
%% 
% * el valor del paso anterior. 
% * el valor calculado por un método explícito, como por ejemplo Euler explícito. 
% (Conocido como  *predictor-corrector*)
%% 
% Por lo tanto, son posibles las siguientes implementaciones:
%% 
% * mieulerimpfix: Iteración simple+dato inicial el valor del paso anterior, 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfix(f,intv,y0,N,TOL,nmax)
%
%% 
% * mieulernwt: Iteración tipo Newton+dato inicial el valor del paso anterior 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpnwt(f,jf,intv,y0,N,TOL,nmax)
%
%% 
% * meulerfixpc: Iteración simple+dato inicial por el método de Euler y que 
% responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax)
%
%% 
% * mieulernwtpc: Iteración tipo Newton+dato inicial por el método de Euler 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfixpc(f,jf,intv,y0,N,TOL,nmax)
%
%% Práctica 2 (El método del trapecio)
% Repetid el ejercicio anterior implementando el método del trapecio
%% Práctica 3 (Ecuación no rígida con Euler implícito)
% Considerad el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haced un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la práctica anterior
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfix* 
%% 
% *Solución*
f = @(t, y) [-2*y(1)+y(2)+2*sin(t);y(1)-2*y(2)+2*(cos(t)-sin(t))];
g = @(t, y) [2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv = [0,10];
y0 = [2,3];
nmax = 10;
TOL = 0.001;
N0 = 200;
h0 = 0.05;
Nvec = N0;
hvec = h0;

for n = 2:8
    Nvec(n) = 2*Nvec(n-1);
    hvec(n) = hvec(n-1)/2;
end

error_elimpfx = NaN;
for n = 1:8
    [t,y] = mieulerimpfix(f,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpfx(n) = error_n;
end

error_elimpfxpc = NaN;
for n = 1:8
    [t,y] = mieulerimpfixpc(f,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpfxpc(n) = error_n;
end

jfunc = [-2,1;1,-2];
error_elimpnwt = NaN;
for n = 1:8
    [t,y] = mieulerimpnwt(f,jfunc,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpnwt(n) = error_n;
end

error_elimpnwtpc = NaN;
for n = 1:8
    [t,y] = mieulerimpnwtpc(f,jfunc,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpnwtpc(n) = error_n;
end

%% 
% %% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfixpc*
%% 
% *Solución*
figure(1)
p = loglog(hvec,error_elimpfx,hvec,error_elimpfxpc);
legend("Metodo de Euler imp Fix","Metodo de Euler imp Fix+pc")
p(1).Marker = '*';
p(2).Marker = 'diamond';
s = sprintf('Sistema no rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden EulImpFij=-0.90648 EulImpPc=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')

%% 
% * comparando los métodos de *mieulerimpnwtpc*, *mieulerimpnwtpc*
%% 
% *Solución*
figure(2)
p1 = loglog(hvec,error_elimpfxpc,hvec,error_elimpnwt);
legend("Metodo de Euler Fix+pc","Metodo de Euler imp Nwt")
p1(1).Marker = '*';
p1(2).Marker = 'diamond';
s = sprintf('Sistema no rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden EulImpFij+Pc=-0.90648 EulImpNwt=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')

figure(3)
p2 = loglog(hvec,error_elimpnwt,hvec,error_elimpnwtpc);
legend("Metodo de Euler imp Nwt","Metodo de Euler imp Nwt+pc")
p2(1).Marker = '*';
p2(2).Marker = 'diamond';
s = sprintf('Sistema no rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden EulImpNwt=-0.90648 EulNwtPc=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')
%% 
% * calcula la pendiente de las rectas
%% 
% *Solución*

%% Práctica 4 (Ecuación no rígida con el trapecio)
% Repetid la práctica 3 pero con el método del trapecio de la práctica 2.
% 
% % 
% *Solución*

error_trapnwt = NaN;
for n = 1:8
    [t,y] = mitrapnwt(f,jfunc,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_trapnwt(n) = error_n;
end

error_trapnwtpc = NaN;
for n = 1:8
    [t,y] = mitrapnwtpc(f,jfunc,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_trapnwtpc(n) = error_n;
end
figure(4)
p2 = loglog(hvec,error_trapnwt,hvec,error_trapnwtpc);
legend("Metodo de Trap nwt","Metodo de Trap Nwt+pc")
p2(1).Marker = '*';
p2(2).Marker = 'diamond';
s = sprintf('Sistema no rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden TrapNwt=-0.90648 TrapNwtPc=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')


%% Práctica 5 (Ecuación rígida con Euler implícito)
% Considerad el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haced un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la práctica anterior
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfix* 
%% 
% *Solución*
f1 = @(t,y) [-2*y(1) + y(2) + 2*sin(t); 998*y(1) - 999*y(2) + 999*(cos(t) - sin(t))];

error_elimpfx1 = NaN;
for n = 1:8
    [t,y] = mieulerimpfix(f1,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpfx1(n) = error_n;
end

error_elimpfxpc1 = NaN;
for n = 1:8
    [t,y] = mieulerimpfixpc(f1,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpfxpc1(n) = error_n;
end

figure(5)
p = loglog(hvec,error_elimpfx1,hvec,error_elimpfxpc1);
legend("Metodo de Euler imp Fix","Metodo de Euler imp Fix+pc")
p(1).Marker = '*';
p(2).Marker = 'diamond';
s = sprintf('Sistema rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden EulImpFij=-0.90648 EulImpPc=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')

jfunc1 = [-2,1;998,-999];
error_elimpnwt1 = NaN;
for n = 1:8
    [t,y] = mieulerimpnwt(f1,jfunc1,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpnwt1(n) = error_n;
end

error_elimpnwtpc1 = NaN;
for n = 1:8
    [t,y] = mieulerimpnwtpc(f1,jfunc1,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_elimpnwtpc1(n) = error_n;
end

figure(6)
p2 = loglog(hvec,error_elimpnwt1,hvec,error_elimpnwtpc1);
legend("Metodo de Euler imp Nwt","Metodo de Euler imp Nwt+pc")
p2(1).Marker = '*';
p2(2).Marker = 'diamond';
s = sprintf('Sistema rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden EulImpNwt=-0.90648 EulNwtPc=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')


%% 
% * calcula la pendiente de las rectas
%% 
% *Solución*
%% Práctica 6 (Ecuación rígida con el trapecio)
% Repetid la práctica 5 pero con el método del trapecio de la práctica 2.
% 
% *Solución *
% 

error_trapnwt1 = NaN;
for n = 1:8
    [t,y] = mitrapnwt(f1,jfunc1,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_trapnwt1(n) = error_n;
end

error_trapnwtpc1 = NaN;
for n = 1:8
    [t,y] = mitrapnwtpc(f,jfunc1,intv,y0,Nvec(n),TOL,nmax);
    dif = y - g(t,y);
    d1 = abs(dif(1,:));
    d2 = abs(dif(2,:));
    error_n = max(max(d1),max(d2));
    error_trapnwtpc1(n) = error_n;
end
figure(7)
p2 = loglog(hvec,error_trapnwt,hvec,error_trapnwtpc);
legend("Metodo de Trap nwt","Metodo de Trap Nwt+pc")
p2(1).Marker = '*';
p2(2).Marker = 'diamond';
s = sprintf('Sistema no rigido intv=[0,10],\n Error maximo vs h,\n M=8 N0=200,\n orden TrapNwt=-0.90648 TrapNwtPc=-1.00308,\n Tol=0.001 nmax=10');
title(s)
xlabel('h')
ylabel('Error maximo')



% %% Apéndice: la implementación de las prácticas 1+2






function [teuler,yeuler]=mieulerimpfix(f,intv,y0,N,TOL,nmax) 
yeuler = y0(:);
h = (intv(2)-intv(1))/N;
teuler = intv(1):h:intv(2);
for n=1:N
    yk = yeuler(:,n);
    tk = teuler(n);
    error = TOL + 1;
    k = 0;
    x0 = yk;
    while k < nmax && error > TOL
        x1 = yk + h*f(tk+h,x0);
        error = norm(x1-x0);
        x0 = x1;
        k = k + 1;
    end
    yeuler(:,n+1) = yeuler(:,n)+h*f(teuler(n+1),x0);
end
end

function [teuler,yeuler]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax) 
yeuler = y0(:);
h = (intv(2)-intv(1))/N;
teuler = intv(1):h:intv(2);
for n=1:N
    yk = yeuler(:,n);
    tk = teuler(n);
    error = TOL + 1;
    k = 0;
    yk0 = yk + h*f(tk,yk);
    while k < nmax && error > TOL
        f0 = yk + h*f(tk+h,yk0);
        yk1 = yk + (h/2)*(f(tk,yk)+f(tk+h,f0));
        error = norm(yk0-yk1);
        yk0 = yk1;
        k = k + 1;
    end
    yeuler(:,n+1) = yeuler(:,n)+h*f(teuler(n+1),yk0);
end

end

function [tvals, yvals]=mieulerimpnwt(f,jfunc,intv,y0,N,TOL,nmax)
yvals = y0(:);
h = (intv(2)-intv(1))/N;
tvals = intv(1):h:intv(2);
for n = 1:N
    yk = yvals(:,n);
    tk = tvals(n);
    error = TOL + 1;
    k = 0;
    yk0 = yk+ h*f(tk,yk);
    while k < nmax && error > TOL
        yk1 = yk - jfunc\f(tk,yk0);
        error = norm(yk1-yk0);
        yk0 = yk1;
        k = k +1;
    end
    yvals(:,n+1) = yvals(:,n)+h*f(tvals(n+1),yk0);
end
end

function [tvals, yvals]=mieulerimpnwtpc(f,jfunc,intv,y0,N,TOL,nmax)
yvals = y0(:);
h = (intv(2)-intv(1))/N;
tvals = intv(1):h:intv(2);
for n = 1:N
    yk = yvals(:,n);
    tk = tvals(n);
    error = TOL + 1;
    k = 0;
    yk0 = yk + h*f(tk,yk);
    f0 = yk + h*f(tk+h,yk0);
    yk0 = yk + (h/2)*(f(tk,yk)+f(tk+h,f0));
    while k < nmax && error > TOL
        yk1 = yk  - jfunc\f(tk,yk0);
        error = norm(yk1-yk0);
        yk0 = yk1;
        k = k +1;
    end
    yvals(:,n+1) = yvals(:,n)+h*f(tvals(n+1),yk0);
end

end

function [ttrap,ytrap]=mitrapfix(f,intv,y0,N,TOL,nmax)
ytrap = y0(:);
h = (intv(2)-intv(1))/N;
ttrap = intv(1):h:intv(2);
for n=1:N
    yk = ytrap(:,n);
    tk = ttrap(n);
    error = TOL + 1;
    k = 0;
    yk0 = f(tk,yk0);
    while k < nmax && error > TOL
        yk0 = yk + f(tk,yk0);
        y = f(tk,yk0);
        error = norm(y-yk0);
        k = k + 1;
    end
    ytrap(:,n+1) = ytrap(:,n)+(h/2)*(f(tk,yk)+f(ttrap(n+1),y));
end


end

function [ttrap,ytrap]=mitrapfixpc(f,intv,y0,N,TOL,nmax) 
ytrap = y0(:);
h = (intv(2)-intv(1))/N;
ttrap = intv(1):h:intv(2);
for n=1:N
    yk = ytrap(:,n);
    tk = ttrap(n);
    error = TOL + 1;
    k = 0;
    yk0 = yk + h*f(tk,yk);
    while k < nmax && error > TOL
        f0 = f(tk+h,yk0);
        yk1 = yk + (h/2)*(f(tk,yk)+f0);
        error = norm(yk0-yk1);
        yk0 = yk1;
        k = k + 1;
    end
    ytrap(:,n+1) = ytrap(:,n)+(h/2)*(f(tk,yk)+f(ttrap(n+1),yk1));
end
disp('H4: file: mitrapfixpc Alumno')
end

function [ttrap,ytrap]=mitrapnwt(f,jfunc,intv,y0,N,TOL,nmax)
ytrap = y0(:);
h = (intv(2)-intv(1))/N;
ttrap = intv(1):h:intv(2);
for n = 1:N
    yk = ytrap(:,n);
    tk = ttrap(n);
    error = TOL + 1;
    k = 0;
    yk0 = yk;
    while k < nmax && error > TOL
        yk1 = yk + h*f(tk,(yk0 - jfunc\f(tk,yk0)));
        error = norm(yk1-yk0);
        yk0 = yk1;
        k = k +1;
    end
    ytrap(:,n+1) = ytrap(:,n)+(h/2)*(f(tk,yk) + f(ttrap(n+1),yk1));
end

           
end
function [ttrap,ytrap]=mitrapnwtpc(f,jfunc,intv,y0,N,TOL,nmax)
ytrap = y0(:);
h = (intv(2)-intv(1))/N;
ttrap = intv(1):h:intv(2);
for n = 1:N
    yk = ytrap(:,n);
    tk = ttrap(n);
    error = TOL + 1;
    k = 0;
    yk0 = yk + h*f(tk,yk);
    while k < nmax && error > TOL
        yk1 = yk + h*f(tk,(yk0 - jfunc\f(tk,yk0)));
        error = norm(yk1-yk0);
        yk0 = yk1;
        k = k +1;
    end
    ytrap(:,n+1) = ytrap(:,n)+(h/2)*(f(tk,yk) + f(ttrap(n+1),yk1));
end

end

function [N_vect,Ev_vect,error_vect]=fcomparerrorimpnwt(met,func,jacfunc,intv,y0,N,yexact,M,TOL,nmax);
disp('H4: file: fcomparerrorimpnwt Alumno')
end 

function [N_vect,Ev_vect,error_vect]=fcomparerrorimpfix(met,func,intv,y0,N,yexact,M,TOL,nmax);
disp('H4: file: fcomparerrorimpfix Alumno')
end

function [p,q]=fcalcorden(N_vect,error_vect) 
disp('H4: file: fcalcorden Alumno')
end
